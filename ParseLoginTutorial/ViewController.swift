//
//  ViewController.swift
//  ParseLoginTutorial
//
//  Created by Laurent Meert on 10/06/15.
//  Copyright (c) 2015 Laurent Meert. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import Bolts
import FBSDKCoreKit
import FBSDKLoginKit

class ViewController: UIViewController, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate {

    
    var signUpViewController: PFSignUpViewController! = PFSignUpViewController()
    var loginViewController: PFLogInViewController! = PFLogInViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        println("ViewController OK")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if PFUser.currentUser() == nil {
            self.loginViewController.delegate = self
            self.signUpViewController.delegate = self
            self.loginViewController.signUpController = signUpViewController
            self.loginViewController.fields = PFLogInFields.UsernameAndPassword | PFLogInFields.Facebook | PFLogInFields.LogInButton | PFLogInFields.PasswordForgotten | PFLogInFields.SignUpButton | PFLogInFields.Twitter
            
            self.loginViewController.logInView?.backgroundColor = UIColor(patternImage: UIImage(named: "background.png")!)
            self.presentViewController(loginViewController, animated: true, completion: nil)
            
            var titleLogo = UILabel()
            titleLogo.text = "Login"
           // titleLogo.font =
            
            self.loginViewController.logInView?.logo = titleLogo
            self.loginViewController.delegate = self
            
            var signupLogoTitle = UILabel()
            signupLogoTitle.text = "SignUp"
            self.signUpViewController.signUpView?.logo = signupLogoTitle
            self.signUpViewController.delegate = self
            
            // self.loginViewController.facebookPermissions = NSArray() as [AnyObject]
            
        }
    }
    
    func logInViewController(logInController: PFLogInViewController, shouldBeginLogInWithUsername username: String, password: String) -> Bool
    {
        if !username.isEmpty || !password.isEmpty
        {
            return true
        }
            
        else
        {
            var alertIdentify:UIAlertView = UIAlertView(title: "Welcome", message: "Hello ", delegate: self, cancelButtonTitle: "Continue")
            alertIdentify.show()
            return false
        }
    }
    
    
    func logInViewController(logInController: PFLogInViewController, didLogInUser user: PFUser){
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    
    func logInViewController(logInController: PFLogInViewController, didFailToLogInWithError error: NSError?) {
            println("test")
        }
    
    func logInViewControllerDidCancelLogIn(logInController: PFLogInViewController) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func signUpViewController(signUpController: PFSignUpViewController, shouldBeginSignUp info: [NSObject : AnyObject]) -> Bool {
        var informationComplete: Bool = true
        
        var eInfo = info as NSDictionary
        
        
        for (key,val) in eInfo {
            
            if let field = eInfo.objectForKey(key) as? NSString {
                if field.length == 0 {
                    informationComplete = false
                    break
                }
            }
    
            
        }
        
        if informationComplete == false {
            
            var alertIdentify:UIAlertView = UIAlertView(title: "Welcome", message: "Hello ", delegate: self, cancelButtonTitle: "Continue")
            alertIdentify.show()
            
        }
        
        return informationComplete
    }


    func signUpViewController(signUpController: PFSignUpViewController, didSignUpUser user: PFUser) {
        //self.dismissViewControllerAnimated(true, completion: nil)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func signUpViewController(signUpController: PFSignUpViewController, didFailToSignUpWithError error: NSError?) {
        println("on a raté!")
    }
    
    func signUpViewControllerDidCancelSignUp(signUpController: PFSignUpViewController) {
        println("user dismissed the signupviewcontroller")
    }
    
    
    @IBAction func logOutAction() {
        PFUser.logOut()
        self.presentViewController(self.loginViewController, animated: true, completion: nil)
        var titleLogo = UILabel()
        titleLogo.text = "Login"
        self.loginViewController.logInView?.logo = titleLogo
        self.loginViewController.delegate = self
        
        var signupLogoTitle = UILabel()
        signupLogoTitle.text = "SignUp"
        self.signUpViewController.signUpView?.logo = signupLogoTitle
        self.signUpViewController.delegate = self
    }
    
    
}





